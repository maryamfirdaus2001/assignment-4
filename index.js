function validate() {
    var total =10;
    var score = 0;

    //Get user input
    var q1 = document.forms['quiz']['q1'].value;
    var q2 = document.forms['quiz']['q2'].value;
    var q3 = document.forms['quiz']['q3'].value;
    var q4 = document.forms['quiz']['q4'].value;
    var q5 = document.forms['quiz']['q5'].value;
    var q6 = document.forms['quiz']['q6'].value;
    var q7 = document.forms['quiz']['q7'].value;
    var q8 = document.forms['quiz']['q8'].value;
    var q9 = document.forms['quiz']['q9'].value;
    var q10 = document.forms['quiz']['q10'].value;

    var q=[q1,q2,q3,q4,q5,q6,q7,q8,q9,q10];

    //validation
    for (var i = 0; i < 10; i++) {
        if (q[i] == null || q[i] == '') {
            alert("Oops! Question "+(i+1)+" is needed.");
           return false;
        }
        
    }
    var name = document.forms['quiz']['name'].value;
    //set the correct answer
    var answers = ["d","b","c","a","a","d","a","c","b","b"];

    //check correct answers
    for (var i = 0; i <10; i++) {
        if (q[i] == answers[i]) {
            score++;
        }
    }

    if (score >=0 && score<=4) {
        alert("Keep trying " + name + "! You answered " + score + " out of 10 correctly");
    } else if (score > 4 && score < 10) {
        alert("Way to go " + name + "! You answered " + score + " out of 10 correctly");
    } else if (score == 10) {
        alert("Congratulations " + name + "! You got " + score + " out of 10");
    }
}
